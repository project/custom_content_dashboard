<?php

/**
  @file
**/
drupal_add_js(drupal_get_path('module', 'custom_content_dashboard') . '/js/dashboard.js');

/* Used to display contents. */
function content_dashboard() {
  drupal_add_css(drupal_get_path('module', 'custom_content_dashboard') . '/css/dashboard.css');

  $div = content_dashboard_get_div_top();
  $output = $div;
  $form = drupal_get_form('_content_load_form');
  $output .= drupal_render($form);
  $output .= "<div class = 'result_section'><span>Filter results:</span></div>";
  $output .= content_dashboard_get_data();
  $output .= theme('pager');
  $div = content_dashboard_get_div_bottom();
  $output .= $div;
  unset($_SESSION['dashboard_filter']['taxonomy']);
  return $output;
}

/**
 * Implemencontent_dashboardts hook_form() for contents dashboard.
 * Used to apply filters.
 */
function _content_load_form($form, &$form_state) {
  global $user;

  $status_filter = array();
  if (isset($_GET['status_filter'])) {
    $temp = $_GET['status_filter'];
    foreach ($temp as $key => $value) {
      $status_filter[$value] = $value;
    }
  }

  $value_array = content_dashboard_get_status();

  $default_title = -1;
  $support_text = '';

  $uname = $user->name;
  $taxonomy_vocabulary = db_select('taxonomy_vocabulary', 'tv')
      ->fields('tv', array('name', 'vid'))
      ->execute()
      ->fetchAll();

  $vocabulary['-1'] = '--select--';
  foreach ($taxonomy_vocabulary as $key => $value) {
    $vocabulary[$value->vid] = $value->name;
  }

  $result = db_select('node_type', 'n')
      ->fields('n', array('type', 'name'))
      ->execute()
      ->fetchAll();

  $content_type = 'all';
  $content_types[''] = '--All--';
  $title = array('-1' => '--Select--', 'title' => 'Title', 'url' => 'URL', 'id' => 'ID');

  foreach ($result as $key => $value) {
    $content_types[$value->type] = $value->name;
  }
  $selected = isset($form_state['values']['taxonomy']) ? $form_state['values']['taxonomy'] : '';
  $tax = $selected;
  $level = array();
  $default_assign = array('Assigned to Me(' . $uname . ')' => 0);
  if (!isset($_SESSION['dashboard_filter']['hid'])) {
    $_SESSION['dashboard_filter']['hid'] = '';
  }

  if (isset($_SESSION['dashboard_filter']['content_type']) && !empty($_SESSION['dashboard_filter']['content_type'])) {
    $content_type = $_SESSION['dashboard_filter']['content_type'];
  }

  if (isset($_SESSION['dashboard_filter']['title']) && $_SESSION['dashboard_filter']['title']!= -1) {
    $default_title = $_SESSION['dashboard_filter']['title'];
  }

  if (isset($_SESSION['dashboard_filter']['taxonomy']) && !empty($_SESSION['dashboard_filter']['taxonomy'])) {
    $tax = $_SESSION['dashboard_filter']['taxonomy'];
  }

  if (isset($_SESSION['dashboard_filter']['text']) && !empty($_SESSION['dashboard_filter']['text'])) {
    $support_text = $_SESSION['dashboard_filter']['text'];
  }

  if (isset($_SESSION['dashboard_filter']['vocabulary']) && !empty($_SESSION['dashboard_filter']['vocabulary'])) {
    $level = $_SESSION['dashboard_filter']['vocabulary'];
  }

  $form['quick_filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quick Filter'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['quick_filter']['content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#options' => $content_types,
    '#default_value' => $content_type
  );


  $form['quick_filter']['status_value'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#validated' => TRUE,
    '#options' => $value_array,
    '#default_value' => $status_filter,
    '#multiple' => TRUE,
    '#prefix' => '<div class = "attribute_val">',
    '#suffix' => '</div>',
  );

  $form['quick_filter']['title'] = array(
    '#type' => 'select',
    '#options' => $title,
    '#default_value' => $default_title,
  );

  $form['quick_filter']['support_text'] = array(
    '#type' => 'textfield',
    '#default_value' => $support_text,
    '#description' => t('Please select an option and enter text based on search criterion.'),
  );

  $form['advance_filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advance Filter'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advance_filter']['taxonomy'] = array(
    '#type' => 'select',
    '#title' => t('Select vocabulary'),
    '#options' => $vocabulary,
    '#default_value' => $tax,
    '#ajax' => array(
      'callback' => '_ajax_dropdown_callback',
      'wrapper' => 'hierarchical_dropdown',
      'method' => 'replace',
    ),
  );
  $form['advance_filter']['vocabulary'] = array(
    '#title' => t('Select vocabulary'),
    '#type' => 'hierarchical_select',
    '#size' => 1,
    '#prefix' => '<div id="hierarchical_dropdown">',
    '#suffix' => '</div>',
    '#default_value' => $level,
    '#config' => array(
      'module' => 'hs_taxonomy',
      'params' => array(
          'vid' => $tax,
      ),
      'save_lineage' => FALSE,
      'enforce_deepest' => FALSE,
      'resizable' => FALSE,
      'level_labels' => array('status' => FALSE),
      'dropbox' => array(
          'status' => TRUE,
          'limit' => -1,
      ),
      'editability' => array(
          'status' => FALSE,
          'allow_new_levels' => FALSE,
          'max_levels' => 0,
          'item_types' => 0
      ),
      'entity_count' => array(
          'enabled' => 0,
          'require_entity' => 0,
          'settings' => array(
              'count_children' => 0,
              'entity_types' => array(),
          ),
      ),
      'render_flat_select' => FALSE,
    ),
  );

  $form['submit'] = array(
    '#value' => 'Apply Filter',
    '#type' => 'submit',
  );

  $form['reset'] = array(
    '#value' => 'Reset Filters',
    '#type' => 'submit',
    '#submit' => array('content_dashboard_reset_filter'),
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function _content_load_form_validate($form, &$form_state) {
  if (isset($_POST['op'])) {
    if ($_POST['op'] == 'Apply Filter') {
      $title_select = $form_state['values']['title'];
      $support_text = $form_state['values']['support_text'];
      if ($title_select != -1) {
        if ($support_text == '') {
          form_set_error('support_text', t('Please Enter text in textfield'));
        }
      }
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function _content_load_form_submit($form, &$form_state) {
  global $base_url;
  $tabledata = array();
  $taxonomy = $form_state['values']['taxonomy'] == -1 ? '' : $form_state['values']['taxonomy'];
  $type = $form_state['values']['content_type'] == -1 ? '' : $form_state['values']['content_type'];
  $title = $form_state['values']['title'] == -1 ? '' : $form_state['values']['title'];
  $related = $form_state['values']['vocabulary'] == array() ? '' : $form_state['values']['vocabulary'];
  $text = $form_state['values']['support_text'] == '' ? '' : $form_state['values']['support_text'];

  $status_data = $form_state['values']['status_value'] == '' ? '' : $form_state['values']['status_value'];

  $_SESSION['dashboard_filter']['hid'] = $status_data;
  if (count($status_data) > 0) {
      $status_data = implode(',', $status_data);
  }

  $_SESSION['dashboard_filter']['content_type'] = $type;
  $_SESSION['dashboard_filter']['title'] = $title;
  $_SESSION['dashboard_filter']['text'] = $text;
  $_SESSION['dashboard_filter']['taxonomy'] = $taxonomy;
  $_SESSION['dashboard_filter']['vocabulary'] = $related;
  $_SESSION['dashboard_filter']['status_data'] = $status_data;

  drupal_goto($base_url . "/admin/custom_content_dashboard");
}

/**
 * Get list of contents.
 */
function content_dashboard_get_data() {
  $type_selected = '';
  if (!isset($_SESSION['dashboard_filter']['content_type'])) {
    $_SESSION['dashboard_filter']['content_type'] = '';
  }

  $header = array(
    array('data' => t('ID'), 'field' => 'nid'),
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Type'), 'field' => 'type'),
    array('data' => t('Created Date'), 'field' => 'created', 'sort' => 'desc'),
    array('data' => t('Status'), 'field' => 'status'),
    array('data' => t('Operations')),
  );

  $data = array();
  $rows = '';
  $data = db_select('node', 'node');
  $data->fields('node', array('nid', 'title', 'type', 'created', 'status'));

  if (isset($_SESSION['dashboard_filter']['content_type']) && !empty($_SESSION['dashboard_filter']['content_type'])) {
    $type_selected = $_SESSION['dashboard_filter']['content_type'];
    $data = $data->condition('node.type', $type_selected, "=");
  }

  if (isset($_SESSION['dashboard_filter']['title']) && !empty($_SESSION['dashboard_filter']['title'])) {
    $title = $_SESSION['dashboard_filter']['title'];
    $like_text = "%" . $_SESSION['dashboard_filter']['text'] . "%";
    $check_text = $_SESSION['dashboard_filter']['text'];
    if ($check_text != '') {
      switch ($title) {
        case 'title':
          $data = $data->condition('node.title', $like_text, 'LIKE');
          break;
       case 'url':
          $query = db_select('url_alias', 'u')
                    ->fields('u', array('source'));
          $db_or = db_or();
          $db_or->condition('alias', $check_text, '=');
          $db_or->condition('source', $check_text, '=');
          $query = $query->condition($db_or)
                    ->execute()
                    ->fetchAll();
          if (count($query) != 0) {
            $source = $query['0']->source;
            if (strpos($source, 'node') !== FALSE) {
              $source = explode("/", $source);
              $source = $source['1'];
              $data = $data->condition('node.nid', $source);
            }
            elseif (strpos($source, 'taxonomy') !== FALSE) {
              $source = explode("/", $source);
              $source = $source['2'];
              $tidquerry = db_select('taxonomy_index', 'ti')
                          ->fields('ti', array('nid'))
                          ->condition('tid', $source)
                          ->execute()
                          ->fetchAll();
              if (count($tidquerry) > 0) {
                $nid = $tidquerry['0']->nid;
                $data = $data->condition('node.nid', $nid);
              }
              else {
                return "No Result Found";
              }
            }
          }
          else {
            return "No result Found";
          }
          break;
          case 'id':
            $data = $data->condition('node.nid', $check_text);
          break;
        }
      }
    }

    if (isset($_SESSION['dashboard_filter']['taxonomy']) && !empty($_SESSION['dashboard_filter']['taxonomy'])) {
      if (isset($_SESSION['dashboard_filter']['vocabulary'])&& !empty($_SESSION['dashboard_filter']['vocabulary'])) {
        $operator = "AND";
        $refer_vid = $_SESSION['dashboard_filter']['taxonomy'];
        $refer_tids = $_SESSION['dashboard_filter']['vocabulary'];
        $query = "SELECT tti.nid AS nid FROM {taxonomy_index} tti INNER JOIN taxonomy_term_data ttd ON tti.tid = ttd.tid WHERE (ttd.vid = " . $refer_vid . ")";
         foreach ($refer_tids as $key => $value) {
            $child = content_dashboard_get_child($refer_vid, $value);
            $query .=$operator . " (EXISTS( SELECT ti.nid AS tid FROM {taxonomy_index} ti WHERE (ti.tid = ttd.tid) AND (ti.tid IN (" . $child . "))))";
         }
        $resul = db_query($query);
        $records = $resul->fetchAll();
        $array = array();
        foreach ($records as $key => $value) {
            $array = $value->nid;
        }
        if (count($array) > 0) {
            $data = $data->condition('node.nid', $array, 'IN');
        }
        else {
            return "No result Found";
        }
      }
    }

    if (isset($_SESSION['dashboard_filter']['status_data']) && !empty($_SESSION['dashboard_filter']['status_data'])) {
      $status_data = $_SESSION['dashboard_filter']['status_data'];
      $status_data_arry = explode(',', $status_data);

      foreach ($status_data_arry as $value) {
        $actualvalue = $value;
          if ($actualvalue == 'sticky') {
            $data = $data->condition('node.sticky', 1);
          }
          if ($actualvalue == 'not sticky') {
            $data = $data->condition('node.sticky', 0);
          }
          if ($actualvalue == 'published') {
            $data = $data->condition('node.status', 1);
          }
          if ($actualvalue == 'not published') {
            $data = $data->condition('node.status', 0);
          }
          if ($actualvalue == 'promoted') {
            $data = $data->condition('node.promote', 1);
          }
          if ($actualvalue == 'not promoted') {
            $data = $data->condition('node.promote', 0);
          }
      }
    }

    $data = $data->extend('PagerDefault');
    $data = $data->limit(20);
    $data = $data->extend('TableSort');
    $data->orderByHeader($header);
    $data = $data->execute()
              ->fetchAll();

  global $pager_total_items;

  $count = "<div class = 'result_count'>" . $pager_total_items['0'] . " content(s) found!!</div>";
  if (count($data) > 0) {
    foreach ($data as $result) {
      global $base_url;
      $nid = $result->nid;
      $source = 'node/' . $nid;

      $nid = $result->nid;
      $id = $result->nid;

      $edit_link = $base_url . "/node/" . $id . "/edit";
      $delete_link = $base_url . "/node/" . $id . "/delete";
      $path_alias = drupal_get_path_alias('node/' . $id);
      $title = l(t($result->title), $path_alias, array('attributes' => array('target' => '_blank')));

      $type = $result->type;
      $types = node_type_get_types();
      $type_name = $types[$type]->name;

      if ($result->status == 0) {
        $result->status = 'Not published';
      }

      if ($result->status == 1) {
        $result->status = 'Published';
      }

      $edit_operation = "<a href =" . $edit_link . " target = '_blank'><span title ='Edit'></span>Edit</a>";
      empty($edit_operation) ? '' : ($edit_operation .= " | ");
      $edit_operation .= "<a href =" . $delete_link . " target = '_blank'><span title ='Delete'></span>Delete</a>";

      $default_timezone = date_default_timezone_get();
      date_default_timezone_set('EST5EDT');
      $default_timezone = date_default_timezone_get();
      $createddate = date("M d,Y H:m:s", $result->created);

      date_default_timezone_set($default_timezone);

       $rows[] = array(
          $nid,
          $title,
          $type_name,
          $createddate,
          $result->status,
          $edit_operation,
      );
    }
    return $count . theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    return "No Result Found";
  }
}

/**
 * User by taxonomy filter.
 */
function _ajax_dropdown_callback($form, &$form_state) {
    $form_state['rebuild'] = TRUE;
    $form = drupal_rebuild_form("_content_load_form", $form_state);
    return $form['advance_filter']['vocabulary'];
}

/**
 * Used for reset filter.
 */
function content_dashboard_reset_filter($form, &$form_state) {
    global $base_url;
    unset($_SESSION['dashboard_filter']['content_type']);
    unset($_SESSION['dashboard_filter']['title']);
    unset($_SESSION['dashboard_filter']['text']);
    unset($_SESSION['dashboard_filter']['taxonomy']);
    unset($_SESSION['dashboard_filter']['vocabulary']);
    unset($_SESSION['dashboard_filter']['status_data']);
    unset($_SESSION['dashboard_filter']['hid']);
    drupal_goto($base_url . "/admin/custom_content_dashboard");
}

/**
 * Return the child of taxonomy.
 */
function content_dashboard_get_child($arg1, $arg2) {
  $term_ids = $arg2;
  $child = taxonomy_get_tree($arg1, $arg2);
  foreach ($child as $key => $value) {
    $term_ids .="," . $value->tid;
  }
  $term_ids = rtrim($term_ids, ",");
  return $term_ids;
}

/**
 * Return list of status.
 */
function content_dashboard_get_status() {
  $str ='';
  $str = array(
                '[any]' => t('any'),
                'published' => t('Published'),
                'not published' => t('Not Published'),
                'promoted' => t('Promoted'),
                'not promoted' => t('Not Promoted'),
                'sticky' => t('Sticky'),
                'not sticky' => t('Not Sticky')
              );
  return $str;
}

/**
 * Top content div.
 */
function  content_dashboard_get_div_top() {
  global $base_url;
  $result = db_select('node_type', 'n')
              ->fields('n', array('type', 'name', 'description'))
              ->execute()
              ->fetchAll();
  foreach ($result as $key => $value) {
    $content_types[] = array($value->type, $value->name, $value->description);
  }

  $div = "<span id = 'content_link_top'>Create Content</span><div id = 'links_top' style = 'display:none;'>";
  foreach ($content_types as $key => $value) {
    $link = $base_url . "/node/add/" . $value['0'];
    $div .= "<p><h3>" . l(t($value['1']), $link) . "</h3><span>" . $value['2'] . "</span></p>";
  }
  $div .= "</div>";
  return  $div;
}

/**
 * Bottom content div.
 */
function content_dashboard_get_div_bottom() {
  global $base_url;
  $result = db_select('node_type', 'n')
              ->fields('n', array('type', 'name', 'description'))
              ->execute()
              ->fetchAll();
  foreach ($result as $key => $value) {
    $content_types[] = array($value->type, $value->name, $value->description);
  }

  $div = "<span id = 'content_link_bottom'><br>Create Content</span><div id = 'links_bottom' style = 'display:none;'>";
  foreach ($content_types as $key => $value) {
    $link = $base_url . "/node/add/" . $value['0'];
    $div .= "<p><h3>" . l(t($value['1']), $link) . "</h3><span>" . $value['2'] . "</span></p>";
  }
  $div .= "</div>";
  return  $div;
}
