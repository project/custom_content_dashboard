/**
* @file
* dashboard JS functions
*/
jQuery(document).ready(function($){

    jQuery('#content_link_top').click(function() {
      if (jQuery('#links_top').is(':visible')) {
        jQuery('#links_top').hide();
      }
      else {
        jQuery('#links_top').show();
      }
    });

    jQuery('#content_link_bottom').click(function() {
      if (jQuery('#links_bottom').is(':visible')) {
        jQuery('#links_bottom').hide();
      }
      else {
        jQuery('#links_bottom').show();
      }
    });

});
