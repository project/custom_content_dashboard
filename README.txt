INTRODUCTION
------------

This Module is useful for listing the content type.
The Module provides an interface to apply the filters based on the requirements.

You can filter by a content type, content status, title, URL, Id (Node Id) and taxonomy (using advance filter).

REQUIREMENTS
------------

This module requires the following modules:

* Hierarchical Select Taxonomy (https://www.drupal.org/project/hierarchical_select)
 - Enable the 
	"Hierarchical Select"
	"Hierarchical Select Taxonomy"
	
INSTALLING
----------

* Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.

